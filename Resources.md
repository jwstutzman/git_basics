## Introduction
Git is a repository management framework. It allows you to create branches off of the main code and merge them back in. This system allows us to protect our main build and work 
collaboratively on the same project.

## Commands
*There is more than one way to do the same thing in git. So there might be other ways you figure out how to do somethings and thats perfectly fine*
Every command is prefaced with `git`.

Follow these basic steps to get started with any project

1. `clone` -- copies a repo at a give address. example: `git clone https://bitbucket.org/jwstutzman/git_basics.git`
2. `fetch` -- grabs the name of all available branches so that you can change to them on your machine. example: `git fetch`
3. `pull` -- grabs the most recent changes that are on your current branch. example: `git pull`
4. `checkout` -- changes to a given branch. If you have the `-b` flag it will create a new branch with the given name. 
    existing branch example: `git checkout master`
         new branch example: `git checkout -b feature/new_feature_name`
5. Now makes some changes. Go nuts!
6. `status` -- shows a list of all the files that currently have changes in them. `git status`
7. `add` -- stages the changes on your local branch to be commited. Adding a `-A` flag will stage all current changes. example: `git add -A`
8. `commit` -- commits all the staged changes to the branch and makes them ready to be pushed for review. The `-m` flag allows you to add the message in the command.                
    example: `git commit -m "Your notes about the changes in the commit"`
9. `push` -- pushes the current branch to the global repo so that other can work off of it or review it. Using `origin HEAD` will push the current branch that you have checked out. example `git push origin HEAD`

*This last step is incredibly important. This will merge your changes in. You should only merge your code into the develop or master branch after it has been reviewed and tested*
*This is also how you can merge other peoples changes into your branch. If your main branch is master and there have been some updates that were added after you created your branch you can use merge to get them*
10. `merge` -- merges the given branch into the branch you currently have checked out. example: `git merge feature/new_feature_name`.
## get changes that have been added to master since you created your branch
1. checkout the branch you want to merge the changes into `git checkout feature/new_feature_name`
2. merge master into your branch `git merge master`
3. stage changes `git add -A`
4. commit changes `git commit -m "merged master into feature/new_feature_name`
5. push changes to global repo `git push origin HEAD`


## When things go wrong and they most likely will... (Happens to everyone)
http://ohshitgit.com/

